package com.example.skindemo;


import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.lib_skin.SkinLog;
import com.example.lib_skin.SkinManager;
import com.example.lib_skin.callback.SkinStateListener;
import com.example.skindemo.activity.CustomViewActivity;
import com.example.skindemo.activity.DialogActivity;
import com.example.skindemo.activity.DynamicActivity;
import com.example.skindemo.activity.FragmentActivity;
import com.example.skindemo.activity.RecyclerViewActivity;
import com.example.skindemo.activity.SkinBaseActivity;
import com.permissionx.guolindev.PermissionX;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends SkinBaseActivity {

    @Override
    protected void initCreate(Bundle savedInstanceState) {
        /// 申请读写权限
        initPermission(new ArrayList<String>() {{
            add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }});

        /// 状态监听回调
        SkinManager.getInstance().setStateListener(this, new SkinStateListener() {
            @Override
            public void skinStateResultCallBack(SkinManager.State state) {
                SkinLog.i("szj当前回调状态1", state.name());
            }
        });
    }

    @Override
    protected int layoutId() {
        return R.layout.activity_main;
    }


    /*
     * 作者:史大拿
     * 创建时间: 12/28/22 8:14 PM
     * TODO 权限申请
     */
    private void initPermission(List<String> permissions) {
        PermissionX.init(this)
                .permissions(permissions)
                .request((allGranted, grantedList, deniedList) -> {
                    if (!allGranted) {
                        Toast.makeText(this, "没开权限，换不了皮肤", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    /*
     * 作者:史大拿
     * 创建时间: 1/4/23 3:55 PM
     * TODO fragment换肤
     */
    public void fragmentClick(View view) {
        startActivity(new Intent(this, FragmentActivity.class));
    }

    /*
     * 作者:史大拿
     * 创建时间: 1/4/23 3:55 PM
     * TODO recyclerView换肤
     */
    public void recyclerClick(View view) {
        startActivity(new Intent(this, RecyclerViewActivity.class));
    }

    /*
     * 作者:史大拿
     * 创建时间: 1/4/23 4:53 PM
     * TODO 自定义view换肤
     */
    public void customViewClick(View view) {
        startActivity(new Intent(this, CustomViewActivity.class));
    }


    /*
     * 作者:史大拿
     * 创建时间: 1/5/23 9:26 AM
     * TODO 动态换肤
     */
    public void dynamicClick(View view) {
        startActivity(new Intent(this, DynamicActivity.class));
    }

    /*
     * 作者:史大拿
     * 创建时间: 1/5/23 9:43 AM
     * TODO dialog换肤
     */
    public void dialogClick(View view) {
        startActivity(new Intent(this, DialogActivity.class));
    }
}