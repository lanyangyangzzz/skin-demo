package com.example.skindemo.activity;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.RequiresApi;
import androidx.core.content.res.ResourcesCompat;

import com.example.lib_skin.SkinLog;
import com.example.lib_skin.SkinManager;
import com.example.lib_skin.callback.SkinStateListener;
import com.example.skindemo.R;

/**
 * @ClassName: DynamicActivity
 * @Author: 史大拿
 * @CreateDate: 1/5/23$ 9:26 AM$
 * TODO 自定义换肤
 */
public class DynamicActivity extends SkinBaseActivity {

    private TextView mTv;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void initCreate(Bundle savedInstanceState) {

        mTv = findViewById(R.id.tv);
        mTv.setBackground(SkinManager.getInstance().getDrawable("global_skin_drawable_background"));
        mTv.setText(SkinManager.getInstance().getString("global_custom_view_text"));

        /// 状态监听回调
        SkinManager.getInstance().setStateListener(this, new SkinStateListener() {
            @Override
            public void skinStateResultCallBack(SkinManager.State state) {
                SkinLog.i("szj当前回调状态2", state.name());
            }
        });
    }

    @Override
    protected void notifyChanged() {
        Toast.makeText(this, "notifyChanged", Toast.LENGTH_SHORT).show();
        mTv.setBackground(SkinManager.getInstance().getDrawable("global_skin_drawable_background"));
        mTv.setText(SkinManager.getInstance().getString("global_custom_view_text"));
    }

    @Override
    protected int layoutId() {
        return R.layout.activity_dynamic;
    }
}
