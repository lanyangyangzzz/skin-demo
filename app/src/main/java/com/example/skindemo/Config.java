package com.example.skindemo;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.File;

/**
 * @ClassName: Config
 * @Author: 史大拿
 * @CreateDate: 1/4/23$ 2:41 PM$
 * TODO
 */
public class Config {

    // 文件
    private static final String SKIN_PACK = "skin-pack-making-debug.apk";

    // 文件夹
    private static final String FOLDER_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;

    public static final String PATH = FOLDER_PATH + SKIN_PACK;

    public static String getPath(Context context) {
        return PATH;
    }
}
